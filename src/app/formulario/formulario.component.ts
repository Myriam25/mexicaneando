import {Component, OnInit} from '@angular/core';
import {Peon} from "../models/peon";




@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {

  public money: number = 0;
  public  MAX_MONEY: number = 5000;
  public peonesList: Peon[] = [];
  public privado: boolean = false;

  ngOnInit(): void {
    this.peonesList = this.getPeones();
  }

  private getPeones(): Peon[] {
    const peonesList: Peon[] = [
      {imagen: '../assets/FICHAS/1.png', nombre: 'Ajolotito'},
      {imagen: '../assets/FICHAS/2.png', nombre: 'Ajolotito'},
      {imagen: '../assets/FICHAS/3.png', nombre: 'Ajolotito'},
      {imagen: '../assets/FICHAS/4.png', nombre: 'Ajolotito'},
      {imagen: '../assets/FICHAS/5.png', nombre: 'Ajolotito'},
      {imagen: '../assets/FICHAS/6.png', nombre: 'Ajolotito'},
      {imagen: '../assets/FICHAS/7.png', nombre: 'Ajolotito'},
      {imagen: '../assets/FICHAS/8.png', nombre: 'Ajolotito'},
      {imagen: '../assets/FICHAS/9.png', nombre: 'Ajolotito'},
      {imagen: '../assets/FICHAS/10.png', nombre: 'Ajolotito'},
    ]
    return peonesList;
  }


  public startGame(): void {

  }
}
