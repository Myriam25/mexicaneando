import { Component, NgModule } from '@angular/core';
import {Routes, RouterModule, Router, ActivatedRoute, NavigationEnd} from '@angular/router';
import { FormularioComponent } from './formulario/formulario.component';
import { InicioComponent } from './inicio/inicio.component'; // importa el componente creado en el paso 2
import { MenuComponent } from './menu/menu.component';



const routes: Routes = [
    {path:'',component:MenuComponent},

    {
        path:'creditos',
        component: InicioComponent,
    },
    {
        path:'formulario',
        component: FormularioComponent,
    }
]

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
  })
  export class AppRoutingModule {
  
  }